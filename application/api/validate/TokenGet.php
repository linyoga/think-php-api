<?php

namespace app\api\validate;

class TokenGet extends BaseValidate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'code' => 'require|isNoEmpty'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'code' => 'code都不给我，你还想要token，渣男'
    ];
}
