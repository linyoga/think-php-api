<?php

namespace app\api\controller\v1;

use app\api\model\Category as ModelCategory;
use app\lib\exception\CategoryException;

class Category
{
    public function getAllCategories()
    {
        $categories = ModelCategory::all([], 'img');
        if ($categories->isEmpty()) {
            throw new CategoryException([
                'errorCode' => 50000
            ]);
        }
        return $categories;
    }
}
