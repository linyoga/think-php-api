<?php

namespace app\api\model;

use think\Model;

class UserAddress extends BaseModel
{
  protected $autoWriteTimestamp = false;
  //设置隐藏字段
  protected $hidden = ['from', 'delete_time', 'update_time', 'head_img_id', 'topic_img_id'];
}
