<?php

namespace app\api\controller\v1;

use app\api\model\Theme as ModelTheme;
use app\api\validate\IDCollection;
use app\api\validate\IDMustBePostivelnt;
use app\lib\exception\ThemeException;

class Theme
{
    /**
     *@Url /theme?ids=id1,id2...
     *
     * @return 一组Theme模型
     */
    public function getSimpleList($ids = '')
    {
        (new IDCollection())->goCheck();
        $ids = explode(',', $ids);
        $result = ModelTheme::with(['topicImd', 'headImg'])->select($ids);
        if ($result->isEmpty()) {
            throw new ThemeException([
                'errorCode' => 30000
            ]);
        }
        return $result;
    }

    public function  getComplexOne($id)
    {
        (new IDMustBePostivelnt())->goCheck();
        $result = ModelTheme::getThemeWithProducts($id);
        if (!$result) {
            throw new ThemeException([
                'errorCode' => 30000
            ]);
        }
        return $result;
    }
}
