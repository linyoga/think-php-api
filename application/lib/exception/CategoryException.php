<?php

namespace app\lib\exception;

class CategoryException extends BaseException
{
    public $code = 404;
    public $msg = '查询的菜单分类不存在，请稍后再查';
    public $erroeCode = 40000;
}
