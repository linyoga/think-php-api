<?php
/*
 * @Author: Administrator 787838675@qq.com
 * @Date: 2021-05-05 19:26:36
 * @LastEditors: Administrator 787838675@qq.com
 * @LastEditTime: 2023-01-19 22:46:59
 * @FilePath: \tp5\application\route.php
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Route;
//动态查询:version
Route::get('api/:version/pay', 'api/:version.Pay/pay');

Route::get('api/:version/banner/:id', 'api/:version.Banner/getBanner');

Route::get('api/:version/theme', 'api/:version.Theme/getSimpleList');
Route::get('api/:version/theme/:id', 'api/:version.Theme/getComplexOne');

Route::get('api/:version/product/recent', 'api/:version.Product/getRecent');
Route::get('api/:version/product/by_category', 'api/:version.Product/getAllInCategory');
Route::get('api/:version/product/:id', 'api/:version.Product/getOne', [], ['id' => '\d+']);
//路由分组



Route::get('api/:version/category/all', 'api/:version.Category/getAllCategories');



Route::post('api/:version/token/user', 'api/:version.Token/getToken');

Route::post('api/:version/address', 'api/:version.Address/createOrUpdateAddress');
Route::post('api/:version/placeOrder', 'api/:version.Order/placeOrder');
Route::post('api/:version/getPreOrder', 'api/:version.Pay/getPreOrder');
