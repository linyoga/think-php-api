<?php

namespace app\api\model;

use think\Model;

class BannerItem extends BaseModel
{
    //设置隐藏字段
    protected $hidden = ['id', 'img_id', 'banner_id', 'delete_time', 'update_time'];

    //定义一对一关联
    public function img()
    {
        # code...
        return $this->belongsTo('Image', 'img_id', 'id');
    }
}
