<?php
namespace app\index\controller;
use think\Controller;
use think\Cookie;
use think\Session;
use think\Db;

class Index extends Controller
{
     //首页
    public function index()
    {
        //加载首页视图
        return  $this->fetch();
    }

    //登陆
    public function login(){
        return $this->fetch();
    }
     //登陆页面数据处理
     public function login_do(){
        //获取页面传输过来的数据
        $data = input('post.');
        //获取标记
        $flag = Session::get('flag');
        //进行表单验证(场景)
        $loginValidate = validate('UserValidate');
        $result = $loginValidate->scene('edit')->check($data);
        //判断验证是否通过
        if($result){
            //开始查询数据库
            $query = Db::table('bbl_user')->where('phone=:phone')->where('password=:password')
                ->bind([
                    'phone'=>$data['phone'],
                    'password'=>$data['password']
                ])->field('user_id,phone,password,login_at,nickname')->find();
            //判断是否这个数据
            if(!empty($query)){ //如果不为空
                //将数据写入Session
                Session::set('user_id',$query['user_id']);
                Session::set('password',$query['password']);
                Session::set('phone',$query['phone']);
                Session::set('nickname',$query['nickname']);
                //更新用户表的登陆时间
                $qu = Db::table('bbl_user')->where('user_id',$query['user_id'])->update(['login_at'=>time()]);
                if($flag == 1){
                    $url = Cookie::get('url');
                }else{
                    $url = url('user/usercenter','','');
                } 
                   //返回一个json数组
                return json([
                    'code'=>1,
                    'msg'=>'登陆成功',
                    'data'=>$url
                ]);
            }else{
                return json([
                    'code'=>0,
                    'msg'=>'电话号码或者登陆密码错误'
                ]);
            }
        }else{
            return json([
                'code'=>0,
                'msg'=>$loginValidate->getError()
            ]);
        }
    }
     //注册
     public function register(){
        //加载注册页面的视图
        return $this->fetch();
    }
    //注册界面数据处理
    public function register_do(){
        //获取页面传过来的数据
        $data = input('post.');
        //进行验证,利用助手函数创建对象
        $userValidate = validate('UserValidate');
        //调用对象的check方法，进行表单验证，并接收返回值
        $result = $userValidate ->check($data);
        //判断验证是否通过
        if(!$result){ 
            //返回一个json数组，
            return json([
                'code'=>0,
                'msg'=>$userValidate->getError()
            ]);
        }else{ 
            //声明一个数组，并且给数据库里面的值，赋值
            $usr = [
                'nickname'=>$data['nickname'],
                'password'=>$data['password'],
                'phone'=>$data['phone'],
                'login_at'=> time()
            ];
            //将数据插入数据库
            $query = Db::table('bbl_user')->insert($usr);
            //判断是否插入成功
            if($query){ //成功
                return json([
                    'code'=>1,
                    'msg'=>'注册成功'
                ]);
            }else{//失败
                return json([
                    'code'=>0,
                    'msg'=>'注册失败'
                ]);
            }
        }
    }
    //搜索
    public function search(){
        //加载搜索界面视图
        return $this->fetch();
    }
     //处理搜索界面的数据
     public function search_do(){
        //接收页面传输过来的数据
        $data = input('post.');
        //开始查询数据库
        $query = Db::table('bbl_movie_layout');
        //判断查询关键字是否为空
        if(!empty($data['keyword'])){
            //如果不为空,就把关键字加到数据库查询条件里面
            $query = $query->where('movie_name','like','%'.$data['keyword'].'%');
           
        }
        //判断日期是否为空
        if(!empty($data['date'])){
            $query = $query->where('release_date',$data['date']);
        }
        //开始查询，并按照上映时间升序来排序
        $result = $query->order('release_time asc')->select();
       //判断查询到的结果是否为空
       if(!empty($result)){
            //如果查询到的结果不为空
            $arrlist = [];
            foreach($result as $itme){
                //统计订票表里面每个场次的余票
                $tickets = Db::table('bbl_movie_ticket')->where('layout_id',$itme['layout_id'])->where('user_id',0)->count();
                //更新排期表里面的余票信息
                $updateresult =  Db::table('bbl_movie_layout')->where('layout_id',$itme['layout_id'])->update(['tickets'=>$tickets]);
                //转换数据库里面的时间，用来于当前时间比较
                $rtime = strtotime($itme['release_date']."".$itme['release_time']);
                //如果当前时间+10*60大于等于数据库里面存储的时间，就跳过本次循环
                if(time()+10*60 >= $rtime){
                    continue;
                }
                //如果余票大于0，则显示购票链接
                if($tickets > 0){
                    $url = url('order/order',['id'=>$itme['layout_id']],'');
                    $url = "<a href='$url'>立即订购</a>";
                }else{ 
                    $url = "";
                }
                $a = array_merge($itme,[
                    'url'=>$url
                ]);
                //添加a数组到$arrlist这个数组中
                array_push($arrlist,$a);
            }
            // dump($arrlist);
            // abort(404);
            //返回一个json对象
            return json([
                'code'=>1,
                'msg'=>'success',
                'data'=>$arrlist
            ]);
       }else{
           //如果没有查询相应的结果
            return json([
                'code'=>0,
                'msg'=>'没有合适的排期'
            ]);
       }
        
    }
}
