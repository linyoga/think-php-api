<?php

namespace app\api\model;

class Image extends BaseModel
{
  //设置隐藏字段
  protected $hidden = ['id', 'from', 'delete_time', 'update_time'];

  //getUrlAttr,Url指的是数据库的字段名，可以指定任意字段名
  public function getUrlAttr($value, $data)
  {
    return $this->prefixImgUrl($value, $data);
  }
}
