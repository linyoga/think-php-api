<?php

namespace app\lib\exception;
//覆盖内容

class BannerMissException extends BaseException
{
    public $code = 404;
    public $msg = '请求的Banner不存在';
    public $erroeCode = 40000;
}
