<?php
namespace app\index\controller;

use think\Controller;
use think\Db;
use think\Session;
use think\Cookie;

class Order extends Controller{
    //前置操作，判断用户是否登陆了
    protected $beforeActionList = [
        'first',
    ];
    //这个方法是判断用户是否登陆了
    protected function first(){
        //判断Session里是否存在当前用户
        if(!Session::has('user_id')){
            Session::set('flag',1);
            Cookie::set('url',request()->url());
            $this->redirect('index/login','',302);
        }
    }

    //加载选座页面
    public function order(){
        //接收电影的排期数
        $layout_id = input('id');
        //查询订票表，查询本次排期的票
        $query = Db::table("bbl_movie_ticket")->where('layout_id',$layout_id)->field('ticket_id,user_id,row,col')->select();
        //将返回的数组，分割成二维数组,
        $seat = [];
        $seat = array_chunk($query,15);
        //赋值视图中的变量
        $this->assign('layout_id',$layout_id);
        $this->assign('seat',$seat);
        return $this->fetch('order');
    }

    //选座结果
    public function seat_do(){
        //获取传输过来的数组
        $data = input('post.'); 
        // dump($data);
        // abort(404);
        //获取Session中储存的用户id
        $user_id = Session::get('user_id');
        //用来保存购票状态
        $result = [];
        //购票时间,本批次
        $tm = time();
        foreach($data["seat"] as $item){
            //将字符串分解成功数组
            $temp = explode('_',$item);
            //更新订票表的订票数据
            $query = Db::table("bbl_movie_ticket")->where('ticket_id',$temp[1])->where('layout_id',$data['layout_id'])
                ->update([
                    'user_id'=>$user_id,
                    'time_at'=>$tm,
                ]);
            // 记录订座成功的座位记录的ticket_id主键
            if($query > 0){
                array_push($result,$temp[1]);
            }
        }
        //判断$result数组是否为空
        if(!empty($result)){ //如果不为空
            //统计订票表里面每个场次的余票
            $tickets = Db::table('bbl_movie_ticket')->where('layout_id',$data['layout_id'])->where('user_id',0)->count();
            //更新排期表里面的余票信息
            $updateresult =  Db::table('bbl_movie_layout')->where('layout_id',$data['layout_id'])->update(['tickets'=>$tickets]);
            // 将本次订座ticket_id，使用_连接起来，作为ids的值传参。
            // true，表示带上完整的域名
            $url = url('order/tickets',['id'=>$data['layout_id'],'tm'=>$tm],'',true);
            //返回json对象
            return json([
                'code'=>1,
                'msg'=>'购票成功',
                'data'=>$url
            ]);
        }else{
            return json([
                'code'=>0,
                'msg'=>'购票失败'
            ]);
        }
    }

    //本次订座的结果页面
    public function tickets(){
        //获取电影的排期
        $layout_id = input("id");
        //获取电影的购票时间
        $tm = input('tm');
        //获取用户的id
        $user_id = Session::get('user_id');
        //查询数据库
        //获取票的数据 
        $result  = Db::table('bbl_movie_ticket')->where('layout_id',$layout_id)->where('time_at',$tm)->where('user_id',$user_id)->select();
        //获取电影的数据 
        $movie_result = Db::table('bbl_movie_layout')->alias('la')
                ->join('bbl_movie_ticket ti','ti.layout_id = la.layout_id')
                ->where('ti.layout_id',$layout_id)->where('ti.time_at',$tm)
                ->field('la.movie_name,la.release_date,la.release_time,la.release_room,la.layout_id,ti.time_at')->find();
        //赋值视图变量
        $this->assign('result',$result);
        $this->assign('movie_result',$movie_result);
        // $this->assign('release_date',$release_date);
        return $this->fetch();
    }

    //下载电影票
    public function download(){
        //获取页面传输过来的值
        $layout_id = input("layout_id");
        $tiem_at = input("time_at");
        //开始查询对应的信息，票的数据
        $result = Db::table("bbl_movie_ticket")->alias('ti')->join("bbl_movie_layout la","la.layout_id = ti.layout_id")
            ->where('ti.layout_id',$layout_id)->where('ti.time_at',$tiem_at)->where('status','待打印')->select();
        //判断查询到数据是否为空
        if(empty($result)){
            return json([
                'code'=>0,
                'msg'=>"该电影票已经下载过了"
            ]);
        }
        $str_3 = null;
        //生成需要写入文本的信息
        foreach($result as $item){
            $movie_name = $item['movie_name'];
            $release_date = $item['release_date'];
            $release_time = $item['release_time'];
            $release_room = $item['release_room'];
            $str = $item['row'];
            $str_2 = $item['col'];
            $str_3 .= $str."排".$str_2."座\n";
        }
        $ticket_txt = <<< EOF
        此订单包含以下内容:
        电影名称:{$movie_name}
        上映日期:{$release_date}
        上映时间:{$release_time}
        上映影厅:{$release_room}
        座位信息:{$str_3};
EOF;
        $dir = ROOT_PATH .'public'.DS.'export';
        $file = "firm_".date("Ymd").".txt";
        $flag = \file_put_contents($dir.DS.$file,$ticket_txt);

        $url = request()->root(true);
        //更新订票表，改变票的打印状态
        $update_ticket = Db::table("bbl_movie_ticket")->where('layout_id',$layout_id)->where('time_at',$tiem_at)
                ->update(['status'=>'已完成']);
        //判断是否写入成功
        if($flag > 0){
            if(stripos($url,'index.php') !== false){
                $url = str_replace('index.php','',$url);
            }
            $url = $url.'export/'.$file;
            return json([
                'code'=>1,
                'msg'=>"下载成功",
                'data'=>[
                    'url'=>$url,
                    'file'=>$file
                ]
            ]);
        }else{
            $url = '';
            return json([
                'code'=>0,
                'msg'=>"下载失败",
                'data'=>[
                    'url'=>$url,
                    'file'=>$file
                ]
            ]);
        }
    }

    //取消定单
    public function cancel(){
        $layout_id = input("layout_id");
        $tiem_at = input("time_at");
        //查询数据库
        $result = Db::table('bbl_movie_ticket')->where('layout_id',$layout_id)->where('time_at',$tiem_at)->where('status','待打印')->select();
        if(!empty($result)){
            foreach($result as $item){
                $a = Db::table('bbl_movie_ticket')->where('layout_id',$item['layout_id'])->where('time_at',$item['time_at'])
                    ->update([
                        'user_id'=>0,
                        'time_at'=>0,
                    ]);
            }
            return json([
                'code'=>1,
                'msg'=>'取消成功'
            ]);
        }else{
            return json([
                'code'=>0,
                'msg'=>'取消失败'
            ]);
        }
        //统计订票表场次的余票
        $tickets = Db::table('bbl_movie_ticket')->where('layout_id',$layout_id)->where('user_id',0)->count();
        //更新排期表里面的余票信息
        $updateresult =  Db::table('bbl_movie_layout')->where('layout_id',$layout_id)->update(['tickets'=>$tickets]);
    }
}