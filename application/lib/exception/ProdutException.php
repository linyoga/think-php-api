<?php

namespace app\lib\exception;

class ProdutException extends BaseException
{
    public $code = 404;
    public $msg = '请求商品不存在，请检查参数';
    public $erroeCode = 30000;
}
