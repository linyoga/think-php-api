<?php
namespace app\index\validate;

use think\Validate;

class CinemaTicketValidate extends Validate{
    //验证和打印电影票
    protected $rule = [
        'phone'  =>  'require|max:11|min:11',  
        'password'  =>  'require|max:11|min:6', 
    ];
    protected $message =[
        'phone.require'=>'电话必填',
        'phone.max' =>'电话号码最多只能是11个字符',
        'phone.min' =>'电话号码最少要11个字符',
        'password.require'=>'密码必填',
        'password.max' =>'密码最多只能是11个字符',
        'password.min' =>'密码最少要6个字符',
        
    ];
    protected  $scene = [
        'edit' => ['phone','password'], 
    ];
}