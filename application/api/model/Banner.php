<?php

namespace app\api\model;

use think\Model;

class Banner extends BaseModel
{
    //设置隐藏字段
    protected $hidden = ['delete_time', 'update_time'];

    //关联模型 一对多关联
    public function items()
    {
        return $this->hasMany('BannerItem', 'banner_id', 'id');
    }
    //静态方法
    public static function getBannerByID($id)
    {
        //TODO:根据Banner ID号获取Banner信息
        $banner = Banner::with(['items', 'items.img'])->find($id);
        return $banner;
    }
}
