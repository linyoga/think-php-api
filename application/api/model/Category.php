<?php

namespace app\api\model;

class Category extends BaseModel
{
    protected $hidden = ['id', 'from', 'delete_time', 'update_time'];
    public function img()
    {
        return $this->belongsTo('image', 'topic_img_id', 'id');
    }
}
