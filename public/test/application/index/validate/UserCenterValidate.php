<?php
namespace app\index\validate;

use think\Validate;

class UserCenterValidate extends Validate{
    //用心中心登陆验证
    protected $rule = [
        'nickname'  =>  'require|max:11|min:3',
        'password'  =>  'max:11|min:6', 
        'password_2'  =>  'confirm:password', 
    ];
    protected $message =[
        'nickname.require'=>'昵称必填',
        'nickname.max' =>'昵称最多只能是11个字符',
        'nickname.min' =>'昵称最少要3个字符',
        'password.max' =>'密码最多只能是11个字符',
        'password.min' =>'密码最少要6个字符',
        'password_2.confirm' =>'前后密码不一致',
    ];
     protected  $scene = [
        'edit' => ['nickname','password','password_2']
    ];
}