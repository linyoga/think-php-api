<?php
namespace app\api\controller;

use think\Controller;
use app\api\service\Token;


class Base extends Controller
{
    //前置方法
    public function checkPrimaryScope(){
        Token::needPrimaryScope();
    }

    public function checkExclusiveScope(){
        Token::needExclusiveScope();
    }
      
}
