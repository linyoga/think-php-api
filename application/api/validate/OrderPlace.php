<?php

namespace app\validate;

use app\api\validate\BaseValidate;
use app\lib\exception\ParameterException;

//购买订单数据校验
class OrderPlace extends BaseValidate
{
    // 传入数据格式模拟
    //二维数组
    protected $products = [
        [
            'produts_id' => 1,
            'count' => 2
        ],
        [
            'produts_id' => 2,
            'count' => 2
        ],
        [
            'produts_id' => 3,
            'count' => 2
        ]
    ];

    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'products' => 'checkProducts'
    ];

    //$singleRlue是$rule下的子数组
    protected $singleRlue = [
        'produts_id' => 'number|integer',
        'count' => 'number|integer'
    ];


    protected function checkProducts($values)
    {
        //判断传入参数是否为数组
        //判断传入参数是否为空
        //遍历验证传入的参数
        if (!is_array($values)) {
            throw new ParameterException([
                'msg' => '商品参数不能为空'
            ]);
        }

        if (empty($values)) {
            throw new ParameterException([
                'msg' => '商品列表不能为空'
            ]);
        }

        //调用checkProduct。遍历value
        foreach ($values as  $value) {
            //$values是用户传递过来的数组，$value为$values下的子元素
            $this->checkProduct($value);
        }
        return true;
    }

    //封装singleRlue验证方法
    protected function checkProduct($value)
    {
        //使用Validate基础写法 调用singleRlue验证规则
        $validate = new BaseValidate($this->singleRlue);
        $result = $validate->check($value);
        if (!$result) {
            throw new ParameterException([
                'msg' => '商品参数错误'
            ]);
        }
    }
}
