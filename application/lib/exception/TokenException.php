<?php

namespace app\lib\exception;

class TokenException extends BaseException
{
    public $code = 401;
    public $msg = 'Token已过期或Token已失效';
    public $erroeCode = 10001;
}
