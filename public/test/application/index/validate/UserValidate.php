<?php
namespace app\index\validate;

use think\Validate;

class UserValidate extends Validate{
    //注册页面数据验证
    protected $rule = [
        'nickname'  =>  'require|max:11|min:3',
        'phone'  =>  'require|max:11|min:11',  
        'password'  =>  'require|max:11|min:6', 
        'password_2'  =>  'require|confirm:password', 
    ];
    protected $message =[
        'nickname.require'=>'昵称必填',
        'nickname.max' =>'昵称最多只能是11个字符',
        'nickname.min' =>'昵称最少要3个字符',
        'phone.require'=>'电话号码必填',
        'phone.max' =>'电话号码最多只能是11个字符',
        'phone.min' =>'电话号码最少要11个字符',
        'password.require'=>'密码必填',
        'password.max' =>'密码最多只能是11个字符',
        'password.min' =>'密码最少要6个字符',
        'password_2.require' =>'重复密码必填',
        'password_2.confirm' =>'前后密码不一致',
        
    ];
    //登陆页面场景验证
    protected  $scene = [
        'edit' => ['phone','password'], 
    ];
}