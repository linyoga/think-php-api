<?php
namespace app\index\controller;

use think\Controller;
use think\Session;
use think\Db;


class CinemaTicket extends Controller{
    // 前置方法，判断是否登陆
    protected $beforeActionList =[
        'first',
    ];
    protected function first(){
        if(Session::has('user_id') == null){
            $this->redirect('cinema_ticket_login/cinematicketlogin','',302);
        }
    }
    //待打印电影票信息
    public function cinematicket(){
        // 获取Session里面的user_id
        $user_id = Session::get('user_id');
        // 开始查询数据库
        $result = Db::table('bbl_movie_ticket')->alias('ti')->join("bbl_movie_layout la","la.layout_id=ti.layout_id")
                ->where('user_id',$user_id)->where('status',"待打印")->order("la.release_time ASC")->select();
        //判断$result是否为空
        if(!empty($result)){
            $this->assign('data', $result);
        }else{
            //为空
            $this->assign('data',"没有待打印的票");
        }
        return $this->fetch();
    }

    //下载电影票
    public function down_link(){
        //获取传输过来的数据
        $layout_id = input('id');
        //开始分割字符串比如将1_1_1，分割成1,1,1
        $data = explode('_',$layout_id);
        //查询数据库
        $result = Db::table("bbl_movie_ticket")->alias("ti")->join("bbl_movie_layout la","ti.layout_id = la.layout_id")
            ->where("ti.layout_id",$data[0])->where("ti.row",$data[1])->where("ti.col",$data[2])->where('ti.status','待打印')->find();

        //判断查询到数据是否为空      
        if(empty($result)){
            return json([
                'code'=>0,
                'msg'=>"该电影票已经下载过了"
            ]);
        }
        $txt = <<< EOF
        此订单包含以下内容：
        电影名称：{$result['movie_name']}
        上映时间：{$result['release_time']}
        上映日期：{$result['release_date']}
        上映影厅：{$result['release_room']}
        座位信息：{$result["row"]}排{$result["col"]}列;
EOF;
        $dir = ROOT_PATH.'public'.DS.'export'; 
        $file =  "firm_" .date("Ymd").".txt";
        $flag = \file_put_contents($dir.DS.$file,$txt);

        $url = request()->root(true);

        //更新订票表，改变票的打印状态
        $update_ticket = Db::table("bbl_movie_ticket")->where('layout_id',$data[0])->where('row',$data[1])->where('col',$data[2])
           ->update(['status'=>'已完成']);

        //判断是否写入成功
        if($flag > 0){
            if(stripos($url,'index.php') !== false){
                $url = str_replace('index.php','',$url);
            }
            $url = $url.'export/'.$file;
            return json([
                'code'=>1,
                'msg'=>'下载成功',
                'data'=>[
                    'url'=>$url,
                    'file'=>$file
                ]
            ]);
        }else{
            return json([
                'code'=>0,
                'msg'=>'下载失败',
                'data'=>[
                    'url'=>$url,
                    'file'=>$file
                ]
            ]);
        }
    }
}