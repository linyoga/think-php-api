<?php
/*
 * @Author: Administrator 787838675@qq.com
 * @Date: 2023-01-19 22:32:49
 * @LastEditors: Administrator 787838675@qq.com
 * @LastEditTime: 2023-01-22 22:11:08
 * @FilePath: \tp5\application\api\controller\v1\pay.php
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

namespace app\api\controller\v1;

use app\api\config\WxPayService;
use app\api\controller\Base;
use app\api\validate\IDCollection;
use think\Exception;
use app\api\service\Pay as PayService;

class Pay 
{
    protected $beforeActionList = [
        'checkExclusiveScope' => ['olay' => 'getPreOrder']
    ];


    public function pay()
    {
        $wxservice = (new  WxPayService())->builder();
        try {
            $resp = $wxservice
            ->chain('v3/pay/transactions/jsapi')
            ->post(['json' => [
                'mchid'        => '1299456001',
                'out_trade_no' => '20221229115414JING-A00000112II',
                'appid'        => 'wx64c896b5fda7f29a',
                'description'  => 'Image形象店-深圳腾大-QQ公仔',
                'notify_url'   => 'https://www.weixin.qq.com/wxpay/pay.php',
                'amount'       => [
                    'total'    => 1,
                    'currency' => 'CNY'
                ],
                'payer'=>[
                    'openid'=>'oqFAE5ryeUA2KTN6qF1YLwueMdm4'
                ]
            ]]);
        
            echo $resp->getStatusCode(), PHP_EOL;
            echo $resp->getBody(), PHP_EOL;

        } catch (Exception $e) {
            // 进行错误处理
            echo $e->getMessage(), PHP_EOL;
            if ($e instanceof \GuzzleHttp\Exception\RequestException && $e->hasResponse()) {
                $r = $e->getResponse();
                echo $r->getStatusCode() . ' ' . $r->getReasonPhrase(), PHP_EOL;
                echo $r->getBody(), PHP_EOL, PHP_EOL, PHP_EOL;
            }
            echo $e->getTraceAsString(), PHP_EOL;
        }
    }

   
    public function getPreOrder($id = '')
    {
        (new IDCollection())->goCheck();
        $pay =new PayService($id);
        $pay->pay();
        
    }
}
