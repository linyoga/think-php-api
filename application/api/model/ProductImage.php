<?php

namespace app\api\model;

use think\Model;

class ProductImage extends BaseModel
{
    //设置隐藏字段
    protected $hidden = ['from', 'delete_time', 'update_time', 'head_img_id', 'topic_img_id', 'pivot', 'create_time', 'category_id', 'img_id', 'product_id'];

    //定义一对一关联
    public function imgUrl()
    {
        # code...
        return $this->belongsTo('Image', 'img_id', 'id');
    }
}
