<?php

namespace app\api\validate;

class IDCollection extends BaseValidate
{

    protected $rule = [
        'ids' => 'require|checkIDs',
    ];

    protected $message = [
        'ids' => 'id必须为逗号分隔的多个正整数',
    ];

    protected function isPositiveInteger($value, $rule = '', $data = '', $field = '')
    {
        if (is_numeric($value) && is_int($value + 0) && ($value + 0) > 0) {
            return true;
        }
        return false;
    }

    protected function checkIDs($value)
    {
        $values = explode(',', $value);
        if (empty($values)) {
            return false;
        }

        foreach ($values as $id) {
            // if (!is_numeric($id)) {
            //     return false;
            // }
            // if (!is_int($id +0)) {
            //     return false;
            // }
            // if (!($id +0)>0) {
            //     return false;
            // }
            // if (!is_numeric($id) and !is_int($id+0) and !($id+0) > 0) {
            //     return false;
            // }
            if (!$this->isPositiveInteger($id)) {
                return false;
            }
        }
        return true;
    }
}
