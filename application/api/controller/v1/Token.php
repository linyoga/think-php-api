<?php
/*
 * @Author: Administrator 787838675@qq.com
 * @Date: 2021-05-05 19:26:36
 * @LastEditors: Administrator 787838675@qq.com
 * @LastEditTime: 2023-01-19 23:00:31
 * @FilePath: \tp5\application\api\controller\v1\Token.php
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

namespace app\api\controller\v1;

use app\api\service\UserToken;
use app\api\validate\TokenGet;

class Token
{
  public function getToken($code = '')
  {
    (new TokenGet())->goCheck();
    // $ut = 1;
    $token = (new UserToken($code))->get();
    //返回一个数组形式，框架自动转换为json格式
    return [
      'token' => $token
    ];
  }
}
