<?php
/*
 * @Author: Administrator 787838675@qq.com
 * @Date: 2021-05-05 19:26:36
 * @LastEditors: Administrator 787838675@qq.com
 * @LastEditTime: 2023-01-22 22:19:42
 * @FilePath: \tp5\application\api\controller\v1\Order.php
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

namespace app\api\controller\v1;

use app\api\service\Order as ServiceOrder;
use app\api\service\Token;
use app\api\validate\OrderPlace;
use app\validate\OrderPlace as ValidateOrderPlace;

use app\api\controller\Base;

//下单是从订单表添加一条数据
class Order extends Base
{
   //用户在选择商品后，向api提交数据，包含用户所选择商品的相关信息
   //api在收到信息后，需要检查订单相关的商品的 库存量
   //有库存，把订单数据存入数据库中(下单成功)，返回客户端消息，告诉客户端可以支付了（待支付）
   //调用支付接口，进行支付
   //支付时需再次检查库存量（支付中）
   //服务器调用微信支付接口，进行支付
   //微信返回支付结果（异步）
   //支付成功：再次检查库存量（支付成功）
   //成功：进行库存量扣除

   protected $beforeActionList = [
      'checkExclusiveScope' => ['only' => 'placeOrder']
   ];

   public function placeOrder()
   {
      //获取用户传递的数据，若是数组需加‘/a’
      (new ValidateOrderPlace())->goCheck();
      $products = input('post.products/a');
      $uid = Token::getCurrentUid();

      $order = new ServiceOrder();
      $status = $order->place($uid, $products);
      return $status;
   }
}