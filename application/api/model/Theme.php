<?php

namespace app\api\model;

class Theme extends BaseModel
{

    //设置隐藏字段
    protected $hidden = ['from', 'delete_time', 'update_time', 'head_img_id', 'topic_img_id'];
    //一对一关联: 
    //1.a表中没有特定的字段关联b表,则在a模型中使用hasone
    //2.a表中有特定的字段关联b表,则在a模型中使用belongsTo

    //主题图
    public function topicImd()
    {
        return $this->belongsTo('Image', 'topic_img_id', 'id');
    }

    //专题列表图，头图
    public function headImg()
    {
        return $this->belongsTo('Image', 'head_img_id', 'id');
    }

    //多对多关联: 
    //belongsToMany
    public function products()
    {
        return $this->belongsToMany('product', 'theme_product', 'product_id', 'theme_id');
    }

    //getComplexOne查询
    //此处返回关联的全部数据
    public static function getThemeWithProducts($id)
    {
        $themes = self::with('products,topicImd,headImg')->find($id);
        return $themes;
    }
}
