<?php
/*
 * @Author: Administrator 787838675@qq.com
 * @Date: 2021-05-05 19:26:36
 * @LastEditors: Administrator 787838675@qq.com
 * @LastEditTime: 2023-01-19 23:08:17
 * @FilePath: \tp5\public\index.php
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]

// 定义应用目录
define('APP_PATH', __DIR__ . '/../application/');
//日志记录目录
define('LOG_PATH', __DIR__ . '/../log/');
// 加载框架引导文件
require __DIR__ . '/../thinkphp/start.php';
//数据库记录
\think\Log::init([
          // 日志记录方式，内置 file socket 支持扩展
          'type'  => 'file',
          // 日志保存目录
          'path'  => LOG_PATH,
          // 日志记录级别
          'level' => ['sql'],
]);