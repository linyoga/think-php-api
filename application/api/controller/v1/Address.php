<?php

namespace app\api\controller\v1;

use app\api\controller\Base;
use app\api\model\User as UserModel;
use app\api\service\Token;
use app\api\validate\AddressNew;
use app\lib\exception\SuccessMessage;
use app\lib\exception\UserException;

class Address extends Base
{

    //前置方法
    //checkPrimaryScope方法存在于父类Base中
    protected $beforeActionList = [
        'checkPrimaryScope' => ['only' => 'createOrUpdateAddress']
    ];

    //删添用户地址
    public function createOrUpdateAddress($jnsj)
    {
        $validate = new AddressNew();
        $validate->goCheck();
        // dump( $validate->goCheck());
        //根据Token来获取UId
        //根据UID查找用户数据，判断用户$user是否存在，如果不存在则抛出异常
        //获取用户从客户端提交的地址信息
        //判断该用户地址信息是否存在，空则添加，否则更新 
        $uid = Token::getCurrentUid();
        $user = UserModel::get($uid);
        if (!$user) {
            throw new UserException();
        }

        //可以先写伪代码，假设以拿到了数据往下写
        // $dataArry = getDatas();

        //根据验证器规定的字段取值
        //获取用户从input传入的数据，对用户数据进行检验
        $dataArry = $validate->getDataByRule(input('post.'));

        //使用关联模型取得user_address表中对应的数据（用户地址）
        $userAddrees = $user->address();
        //判断用户地址是否存在
        if (!$userAddrees) {
            //调用User模型下的关联模型address，新增一条数据
            $user->address()->save($dataArry);
        } else {
            $user->address->save($dataArry);
        }
        // return $user;
        //统一返回格式
        return new SuccessMessage();
    }


    public function deleteOne($id)
    {
    }
}
