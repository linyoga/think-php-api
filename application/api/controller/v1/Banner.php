<?php

namespace app\api\controller\v1;

use app\api\model\Banner as ModelBanner;
use app\api\validate\IDMustBePostivelnt;
use app\lib\exception\BannerMissException;

class Banner
{
    /**
     * 获取指定id的banner信息
     * @url /banner/:id
     * @http GET
     * @id banner的id号
     *
     */
    public function getBanner($id)
    {
        (new IDMustBePostivelnt())->goCheck();
        $banner = ModelBanner::getBannerByID($id);
        if (!$banner) {
            # code...
            throw new BannerMissException();
        }
        return $banner;
    }
}
