<?php
namespace app\index\controller;

use think\Controller;
use think\Db;
use think\Session;

class CinemaTicketLogin extends Controller{
    // 加载验证和打印电影票页面
    public function cinematicketlogin(){
        return $this->fetch();
    }
    //处理验证和打印电影票页面数据
    public function cinematicketlogin_do(){
          //获取页面传输过来的数据 
          $data = input("post.");
          //进行数据验证
          $cinematicket = validate('CinemaTicketValidate');
          $result = $cinematicket->scene('edit')->check($data);
          //判断数据验证是否通过了
          if($result){
              //通过了，开始查询数据库
              $query = Db::table("bbl_user")->where('phone',$data['phone'])->where('password',$data['password'])->field('user_id,phone,password,nickname')->find();
              //判断$query是否为空
              if(!empty($query)){
                  //不为空,将登陆用户的数据写进Session
                  Session::set('user_id',$query['user_id']);
                  Session::set('password',$query['password']);
                  Session::set('phone',$query['phone']);
                  Session::set('nickname',$query['nickname']);
                  // //更新用户的登陆时间
                  $a = Db::table('bbl_user')->where('phone',$data['phone'])->where('password',$data['password'])->update(['register_at'=>time()]);
                  return json([
                      'code'=>1,
                      'msg'=>'登陆成功'
                  ]);
              }else{
                  //为空
                  return json([
                      'code'=>0,
                      'msg'=>"不存在这个用户"
                  ]);
              }
          }else{
              //没有通过 
              return json([
                  'code'=>0,
                  'msg'=>$cinematicket->getError()
              ]);
          }
    }

}