<?php

namespace app\api\controller\v1;

use app\api\model\Product as ModelProduct;
use app\api\validate\Count;
use app\api\validate\IDMustBePostivelnt;
use app\lib\exception\ProdutException;

class Product
{
    //获取商品信息条数
    public function getRecent($conut = 15)
    {
        (new Count())->goCheck();
        $products = ModelProduct::getMostRecent($conut);
        if ($products->isEmpty()) {
            throw new ProdutException([
                'errorCode' => 20000
            ]);
        }
        $products = $products->hidden(['summary']);
        return $products;
    }

    //获取商品分类
    public function getAllInCategory($id)
    {
        (new IDMustBePostivelnt())->goCheck();
        $products = ModelProduct::getProductsByCategoryID($id);
        if ($products->isEmpty()) {
            throw new ProdutException([
                'errorCode' => 20000
            ]);
        }
        $products = $products->hidden(['summary']);
        return $products;
    }

    //商品详情数据
    public  function getOne($id)
    {
        (new IDMustBePostivelnt())->goCheck();
        $product = ModelProduct::getProductDetail($id);
        if (!$product) {
            throw new ProdutException([
                'errorCode' => 20000
            ]);
        }
        $product = $product->hidden(['summary']);
        return $product;
    }
}
