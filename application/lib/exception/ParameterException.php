<?php

namespace app\lib\exception;

// use app\lib\exception\BaseException;

//客户端异常消息定义
class ParameterException extends BaseException
{
    public $code = 400;
    public $msg = '参数错误';
    public $erroeCode = 10000;
}
