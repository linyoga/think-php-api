<?php

namespace app\lib\enum;

class ScopeEnum
{
    //枚举
    //用户权限User   管理权限Super
    const User = 16;
    const Super = 32;
}
