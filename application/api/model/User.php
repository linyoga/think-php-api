<?php
/*
 * @Author: Administrator 787838675@qq.com
 * @Date: 2021-05-05 19:26:36
 * @LastEditors: Administrator 787838675@qq.com
 * @LastEditTime: 2023-01-19 22:56:47
 * @FilePath: \tp5\application\api\model\User.php
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

namespace app\api\model;

use think\Model;

class User extends BaseModel
{
    //关联模型写在前面，模型方法写在后面
    public function address()
    {
        //一对一关联中，无外键关联字段使用hasOne
        return $this->hasOne('UserAddress', 'user_id', 'id');
    }

    public static function getByOpenID($openid)
    {
        //根据openid查找用户
        $user = self::where('openid', '=', $openid)->find();
        if ($user) {
            $uid = $user['id'];
            return $uid;
        } else {
            $user = User::create([
                'openid' => $openid
            ]);
            $uid = $user['id'];
            return $uid;
        }
    }
}
