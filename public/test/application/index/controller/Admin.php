<?php
namespace app\index\controller;

use think\Controller;
use think\Db;

class Admin extends Controller{
     //排期表里插入数据
     public function set_layout(){
        $layout = [
            [
                'movie_name'=>'中国机长',
                'release_date'=> '2020/1/1',
                'release_time'=>'20:30',
                'release_room'=>'1号厅',
                'tickets'=>150,
                'movie_price'=>35.5
            ],
            [
                'movie_name'=>'战狼',
                'release_date'=> '2020/1/13',
                'release_time'=>'19:00',
                'release_room'=>'2号厅',
                'tickets'=>150,
                'movie_price'=>30
            ],
            [
                'movie_name'=>'少年的你',
                'release_date'=> '2020/2/1',
                'release_time'=>'20:30',
                'release_room'=>'3号厅',
                'tickets'=>150,
                'movie_price'=>35.5
            ],
            [
                'movie_name'=>'地久天长',
                'release_date'=> '2020/1/8',
                'release_time'=>'21:30',
                'release_room'=>'4号厅',
                'tickets'=>150,
                'movie_price'=>40
            ],
            [
                'movie_name'=>'哪吒之魔童降世',
                'release_date'=> '2020/1/12',
                'release_time'=>'20:30',
                'release_room'=>'5号厅',
                'tickets'=>150,
                'movie_price'=>35.5
            ],
            [
                'movie_name'=>'我不是药神',
                'release_date'=> '2020/1/15',
                'release_time'=>'20:00',
                'release_room'=>'6号厅',
                'tickets'=>150,
                'movie_price'=>35.5
            ],
            [
                'movie_name'=>'我和我的祖国',
                'release_date'=> '2020/1/12',
                'release_time'=>'20:30',
                'release_room'=>'7号厅',
                'tickets'=>150,
                'movie_price'=>35.5
            ],
            [
                'movie_name'=>'红海行动',
                'release_date'=> '2020/1/15',
                'release_time'=>'20:30',
                'release_room'=>'8号厅',
                'tickets'=>150,
                'movie_price'=>35.5
            ],
        ];

        //将数据插入数据库
        $result = Db::table('bbl_movie_layout')->insertAll($layout);
        dump($result);
    }

    //向订票表插入数据
    public function set_ticket(){
        //查询数据库拿到排期
        $result = Db::table('bbl_movie_layout')->field('layout_id')->select();
        $a = 0;
        foreach($result as $item){
            for($i = 1; $i <= 10;$i++){
                for($j = 1; $j <= 15;$j++){
                  $tmp = [
                      'layout_id'=>$item['layout_id'],
                      'user_id'=>0,
                      'row'=>$i,
                      'col'=>$j,
                  ];
                  Db::table('bbl_movie_ticket')->insert($tmp);
                  $a++;
                }
            }
        }
        dump($a);
    }
}