<?php

namespace app\index\controller;

use think\Controller;
use think\Session;
use think\Cookie;
use think\Db;

class User extends Controller
{
    //前置方法，判断是否登陆
    protected $beforeActionList = [
        'first',
    ];
    protected function first()
    {
        if (Session::has('user_id') == null) {
            Session::set('flag', 0);
            $this->redirect('index/login', '', 302);
        }
    }
    //清除Session里面的内容
    public function esc()
    {
        session(null);
        return $this->success("退出成功", 'index/login', 3);
    }
    //客户中心
    public function usercenter()
    {
        //基本信息
        //获取Session里面的值，并赋值在页面输出，
        $this->assign('nickname', Session::get('nickname'));
        $this->assign('phone', Session::get('phone'));
        //购票信息
        //开始查询数据库
        $user_id = Session::get("user_id");
        $ticketCount = Db::table("bbl_movie_ticket")->where("user_id",$user_id)->distinct("time_at,layout_id")->field("time_at,layout_id")->order("time_at desc")->select();
        // dump($ticketCount);
        $ticketArr =[]; 
        foreach($ticketCount as $itme){
            $result = null;
            $result = Db::table("bbl_movie_ticket")->alias('ti')->join('bbl_movie_layout la','ti.layout_id = la.layout_id')
            ->where("ti.layout_id",$itme["layout_id"])->where("ti.time_at",$itme["time_at"])->select();
            array_push($ticketArr,$result);
        }
        // dump($ticketArr);
        //赋值到页面输出
        $this->assign("ticketArr",$ticketArr);
        //加载客户中心视图
        return $this->fetch();
    }

    //处理用户中心基本信息模块传输过来的数据
    public function usercenter_do()
    {
        //创建验证类对象
        $userValidate = validate('UserCenterValidate');
        //判断密码是否为空
        $password = input('post.password');
        if (empty($password)) {
            //进行数据的场景验证
            $result = $userValidate->scene('edit')->check(input('post.'));
            //判断验证是否通过 
            if ($result) { //通过了，就更新用户表里面的数据
                //判断当前用户修改的用户名是否跟之前的一样
                if (input('post.nickname') == Session::get('nickname')) {
                    //如果一样直接返回json对象
                    return json([
                        'code' => 1,
                        'msg' => "修改成功"
                    ]);
                } else {
                    //如果不一样，则开始更新用户表里面的数据
                    $query = Db::table('bbl_user')->where('user_id', Session::get('user_id'))->update(['nickname' => input('post.nickname')]);
                    if ($query) {
                        //返回一个json对象
                        return json([
                            'code' => 1,
                            'msg' => '修改成功',
                        ]);
                    } else {
                        //返回一个json对象
                        return json([
                            'code' => 0,
                            'msg' => '修改失败'
                        ]);
                    }
                }
            } else {
                //如果验证没有通过
                return json([
                    'code' => 0,
                    'msg' => $userValidate->getError()
                ]);
            }
        } else { //如果密码不为空
            //进行数据场景验证
            $result = $userValidate->scene('edit')->check(input('post.'));
            //判断验证是否通过
            if ($result) {
                //更新数据库里面的数据
                $query = Db::table('bbl_user')->where('user_id', Session::get('user_id'))->update([
                    'nickname' => input('post.nickname'),
                    'password' => input('post.password')
                ]);
                if ($query) {
                    return json([
                        'code' => 1,
                        'msg' => '修改成功'
                    ]);
                } else {
                    return json([
                        'code' => 0,
                        'msg' => '修改失败'
                    ]);
                }
            } else {
                //如果验证没有通过
                return json([
                    'code' => 0,
                    'msg' => $userValidate->getError()
                ]);
            }
        }
    }

    //处理用户中心购票信息模块传输过来的数据
    public function buyticket (){
        //座位的数据
        $data = input("post.ids");
        //用户id
        $user_id = Session::get("user_id");
        $temp = explode("_",$data);
        //开始更新数据库
        $result = Db::table("bbl_movie_ticket")->where("layout_id",$temp[0])
                ->where("time_at",$temp[1])->where('user_id',$user_id)
                ->update(['status'=>'已完成']);
        //判断是否更新成功 
        if($result){  
            return json([
                'code'=>1,
                'msg'=>"出票成功"
            ]);
        }else{
            return json([
                'code'=>0,
                'msg'=>'出票失败'
            ]);
        }
    }
    //处理用户中心购票信息模块传输过来的数据
    public function buyticket_cancel (){
        //座位的数据
        $data = input("post.ids");
        //用户id
        $user_id = Session::get("user_id");
        $temp = explode("_",$data);
        //开始更新数据库
        $result = Db::table("bbl_movie_ticket")->where("layout_id",$temp[0])
                ->where("time_at",$temp[1])->where('user_id',$user_id)
                ->where('status',"待打印")
                ->update(['user_id'=>0,'time_at'=>0]); 
        //判断是否更新成功
        if($result){ 
        //统计订票表场次的余票
         $tickets = Db::table('bbl_movie_ticket')->where('layout_id',$temp[1])->where('user_id',0)->count();
         //更新排期表里面的余票信息
         $updateresult =  Db::table('bbl_movie_layout')->where('layout_id',$temp[1])->update(['tickets'=>$tickets]);
            return json([
                'code'=>1,
                'msg'=>"取消成功"
            ]);
        }else{
            return json([
                'code'=>0,
                'msg'=>'取消失败'
            ]);
        }
    }
}
