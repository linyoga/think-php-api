<?php

namespace app\api\service;

use app\lib\enum\ScopeEnum;
use app\lib\exception\ForbiddenException;
use app\lib\exception\TokenException;
use Exception;
use think\Request;
use think\Cache;

class Token
{
    //不需要传参，令牌配置
    public static function generateToken()
    {
        //使用getRandChanrs方法（方法位于common内），32个字符组成一个随机的字符串    
        //当前时间戳                                                              
        //salt 盐.自定义字符串                                                    
        //用以上三组字符串（ $randChars、 $timeTamp 、 $salt），进行md5加密
        //得到令牌
        $randChars = getRandChar(32);
        $timeTamp = $_SERVER['REQUEST_TIME_FLOAT'];
        $salt = config('scurt.tkoen_salt');
        return md5($randChars . $timeTamp . $salt);
    }

    //根据缓存$key($token令牌)获取$vaule数据
    public static function getCurrentTokenVar($key)
    {
        //获取用户提交的所有数据，所有数据通过header方法获取
        //cache方法用于操作缓存操作,使用token（令牌）获取对应的$value
        //判断缓存是否取到数据
        //不存在则抛出异常
        //判断不为数组则转换为·数组
        //判断$vars($value)是否存在$key(令牌)
        //存在则return（返回）
        $token = Request::instance()->header('token');
        $vars = Cache::get($token);
        if (!$vars) {
            throw new TokenException();
        } else {
            if (!is_array($vars)) {
                $vars = json_decode($vars, true);
            }
            if (array_key_exists($key, $vars)) {
                return $vars[$key];
            } else {
                throw new Exception('尝试获取的Token的变量不存在');
            }
        }
    }

    //从令牌中取得用户UId
    public static function getCurrentUid()
    {
        $uid = self::getCurrentTokenVar('uid');
        return $uid;
    }

    //定义用户与管理员都可以访问的方法
    //self    同级调用
    public static function needPrimaryScope()
    {
        $scope = self::getCurrentTokenVar('scope');
        if ($scope) {
            if ($scope >= ScopeEnum::User) {
                return true;
            } else {
                throw new ForbiddenException();
            }
        } else {
            throw new TokenException();
        }
    }

    //定义仅用户可以访问的方法
    public static function needExclusiveScope()
    {
        $scope = Token::getCurrentTokenVar('scope');
        if ($scope) {
            if ($scope == ScopeEnum::User) {
                return true;
            } else {
                throw new ForbiddenException();
            }
        } else {
            throw new TokenException();
        }
    }

    public static function isValidOperate($checkedUID)
    {
        if (!$checkedUID) {
            throw new Exception("用户UID不存在");
        }
        $currentOperateUID = self::getCurrentUid();
        if ($checkedUID == $currentOperateUID) {
            return true;
        }
        return false;
    }

}
