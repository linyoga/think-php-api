<?php

namespace app\api\validate;

use think\Validate;

class AddressNew extends BaseValidate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'name' => 'require|chsAlpha|length:2,8',
        'mobile' => 'require',
        'province' => 'require',
        'city' => 'require',
        'country' => 'require',
        'detail' => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [];
}
