<?php

namespace app\api\validate;

class IDMustBePostivelnt extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|integer',
        'num' => 'number'
    ];
}
