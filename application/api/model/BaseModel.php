<?php

namespace app\api\model;

use think\Model;

class BaseModel extends Model
{
  //定义函数 //拼接图片地址
  protected function prefixImgUrl($value, $data)
  {
    //from==1 表示图片为本地，需要补全路径
    if ($data['from'] == '1') {
      return config('setting.img_prefix') . $value;
    } else {
      return $value;
    }
  }
}
