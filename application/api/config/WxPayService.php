<?php
/*
 * @Author: Administrator 787838675@qq.com
 * @Date: 2023-01-22 09:50:12
 * @LastEditors: Administrator 787838675@qq.com
 * @LastEditTime: 2023-01-22 22:21:58
 * @FilePath: \tp5.0\application\api\config\WxPay.php
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

namespace app\api\config;

use WeChatPay\Builder;
use WeChatPay\Crypto\Rsa;
use WeChatPay\Util\PemUtil;

class WxPayService
{
    // @$merchantId 商户号
    // @$merchantPrivateKeyFilePath 从本地文件中加载「商户API私钥」，「商户API私钥」会用来生成请求的签名
    // @$merchantCertificateSerial「商户API证书」的「证书序列号」
    // @$platformCertificateFilePath 从本地文件中加载「微信支付平台证书」，用来验证微信支付应答的签名
    private $merchantId;
    private $merchantPrivateKeyFilePath;
    private $merchantCertificateSerial;
    private $platformCertificateFilePath;
    private $appid;

    /**
     * Summary of __construct
     * 初始化微信支付信息
     */
    public function __construct()
    {
        $this->merchantId = '1299456001';
        $this->merchantPrivateKeyFilePath = 'file:///workProject\key\beihan\apiclient_key.pem';
        $this->platformCertificateFilePath = 'file:///workProject\key\beihan\wechatpay_apiv3.pem';
        $this->merchantCertificateSerial = '504CD31D31BE6238D56B076CA10C19FAE0EA9834';
        $this->appid ='wx64c896b5fda7f29a';
    }

    public function getconfig(){
        $config = [
            'merchantId'=>$this->merchantId,
            'appid'=>$this->appid,

        ];
        return $config;
    }


    /**
     * Summary of builder
     * @return \WeChatPay\BuilderChainable
     * 构建微信支付服务
     */
    public function builder()
    {
        $merchantPrivateKeyInstance = Rsa::from($this->merchantPrivateKeyFilePath, Rsa::KEY_TYPE_PRIVATE);
        $platformPublicKeyInstance = Rsa::from($this->platformCertificateFilePath, Rsa::KEY_TYPE_PUBLIC);
        $platformCertificateSerial = PemUtil::parseCertificateSerialNo($this->platformCertificateFilePath);
        // 构造一个 APIv3 客户端实例
        $instance = Builder::factory([
            'mchid' => $this->merchantId,
            'serial' => $this->merchantCertificateSerial,
            'privateKey' => $merchantPrivateKeyInstance,
            'certs' => [
                $platformCertificateSerial => $platformPublicKeyInstance,
            ],
        ]);
        return $instance;
    }

}
