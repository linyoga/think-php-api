<?php

namespace app\lib\exception;

// use app\lib\exception\BaseException;

//客户端异常消息定义
class ForbiddenException extends BaseException
{
    public $code = 403;
    public $msg = '非法权限';
    public $erroeCode = 10001;
}
